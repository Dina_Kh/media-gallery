# Responsive Media Gallery

Create a simple image gallery grid page with an on-click event for the images that opens an overlay pop-up with a short description (A paragraph of three to four lines) about the image clicked, you can use [this API](https://jsonplaceholder.typicode.com/photos) to get the photos.

### What do you need to do

- Divide the page into 4 columns with an unknown number of rows.
- Columns are equal in size with 10px spacing between them and the spacing between rows is always 10px.
- In every column 1 image is displayed with the title below.
- images and titles are aligned in the center of the column, the title is 5px below the image.
- The maximum width of the overlay pop-up is 600px, this pop-up will appear when an image is clicked.
- You are free to add subtle transitions and/or animations.
- All styles are applied using SCSS/CSS.
- Javascript code is written using what you have learned in Javascript and ES6.

### What NOT to use

1. CSS Front-end frameworks such as Bootstrap and Foundation.
2. Javascript libraries and plugins such as jquery.

### What we will look at

1. The validity semantics of the HTML5 created and how are accessibility, usability and SEO considered in the code.
2. The cross browser compatibility (ie Edge and higher).
3. Responsiveness of the page.
4. The structure of the SCSS/CSS.
5. The structure and pattern used in the created JavaScript file/s.
6. The user interface and user experience of the page
7. The structure of the project folders and files.
8. Unit tests for components
9. Code is documented and any steps to get it working are mentioned clearly.
10. SCSS and JS are compiling without errors
11. Code is pushed to GitHub with clear Repo name and commit messages

#### BONUS

- Use WebPack to manage you assets instead of other task runners.
